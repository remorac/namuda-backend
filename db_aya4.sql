-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `db_aya3` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_aya3`;

DROP TABLE IF EXISTS `artikel`;
CREATE TABLE `artikel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `artikel_kategori_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `waktu` date DEFAULT NULL,
  `isi` text NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `artikel_kategori_id` (`artikel_kategori_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `artikel_ibfk_1` FOREIGN KEY (`artikel_kategori_id`) REFERENCES `artikel_kategori` (`id`),
  CONSTRAINT `artikel_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `artikel` (`id`, `judul`, `artikel_kategori_id`, `user_id`, `waktu`, `isi`, `gambar`) VALUES
(3,	'all Happy and Free',	1,	1,	'2017-04-17',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.\r\n</p><p>This group would somehow form a family that\'s the way the Love Boat promises something for everyone that this matt group would somehow form a the brady bunch  that this group they call him Flipper Flipper faster than lightning no one you see is smarter than he maybe you and me think of me once in a while.\r\n</p><blockquote><span class=\"quote-icon flaticon-left-quote\"></span> This group would somehow form a family that\'s the way the Love Boat promises something brady bunch  that this group they call him Flipper Flipper.  <span class=\"quote-icon flaticon-right-quotation-sign\"></span></blockquote><p>The wood somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.\r\n</p><p>The way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch one that this group would some is how form a family that\'s the way.\r\n</p>',	'uploads/artikel3.jpg'),
(4,	'all Happy and Free',	2,	1,	'2017-04-12',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.</p><p>This group would somehow form a family that\'s the way the Love Boat promises something for everyone that this matt group would somehow form a the brady bunch  that this group they call him Flipper Flipper faster than lightning no one you see is smarter than he maybe you and me think of me once in a while.</p><blockquote><div class=\"blockquote-text\"><span class=\"quote-icon flaticon-left-quote\"></span> This group would somehow form a family that\'s the way the Love Boat promises something brady bunch  that this group they call him Flipper Flipper. &nbsp;<span class=\"quote-icon flaticon-right-quotation-sign\"></span></div></blockquote><p>The wood somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.</p><p>The way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch one that this group would some is how form a family that\'s the way.</p>',	'images/resource/news-block-18.jpg'),
(5,	'his beautiful daya',	1,	1,	'2017-04-16',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.</p><p>This group would somehow form a family that\'s the way the Love Boat promises something for everyone that this matt group would somehow form a the brady bunch  that this group they call him Flipper Flipper faster than lightning no one you see is smarter than he maybe you and me think of me once in a while.</p><blockquote><div class=\"blockquote-text\"><span class=\"quote-icon flaticon-left-quote\"></span> This group would somehow form a family that\'s the way the Love Boat promises something brady bunch  that this group they call him Flipper Flipper. &nbsp;<span class=\"quote-icon flaticon-right-quotation-sign\"></span></div></blockquote><p>The wood somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.</p><p>The way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch one that this group would some is how form a family that\'s the way.</p>',	'images/resource/news-block-17.jpg'),
(6,	'tes create',	1,	1,	'2017-06-01',	'<p>testing</p>',	'uploads/artikel6.jpg'),
(8,	'tes create 2',	5,	1,	'2017-06-01',	'<p>siury sru s;hdgj  fsddhgdfjsfhjd</p>',	'uploads/8.png'),
(9,	'Tes Create 3',	5,	1,	'2017-06-03',	'<p>qwertyuiop asdfghjkl zxcvbnm</p>',	NULL),
(10,	'Tes Create 3.1',	2,	1,	'2017-06-06',	'<p>isi</p>',	NULL);

DROP TABLE IF EXISTS `artikel_kategori`;
CREATE TABLE `artikel_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `artikel_kategori` (`id`, `nama`) VALUES
(1,	'Sales Effectiveness'),
(2,	'Leadership Development'),
(3,	'Personal Effectiveness'),
(4,	'Presentation Skills'),
(5,	'Negotiation Training'),
(6,	'Real Deal Coaching'),
(7,	'Programming'),
(8,	'Operating System'),
(10,	'Entrepreneurship'),
(11,	'Religion'),
(12,	'Fiction');

DROP TABLE IF EXISTS `artikel_komentar`;
CREATE TABLE `artikel_komentar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artikel_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `waktu` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `artikel_id` (`artikel_id`),
  CONSTRAINT `artikel_komentar_ibfk_1` FOREIGN KEY (`artikel_id`) REFERENCES `artikel` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `artikel_komentar` (`id`, `artikel_id`, `nama`, `email`, `isi`, `waktu`) VALUES
(1,	5,	'nama',	'email@email.com',	'ini adalah komen',	'2017-10-14 00:00:00'),
(2,	5,	'nama2',	'emal',	'tes koment',	'2017-10-14 00:00:00');

DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin',	'1',	1496398540);

DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel-kategori/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel-kategori/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel-kategori/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel-kategori/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel-kategori/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel-kategori/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel-komentar/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel-komentar/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel-komentar/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel-komentar/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel-komentar/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel-komentar/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/artikel/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/config/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/config/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/config/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/config/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/config/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/config/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/debug/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/debug/default/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/debug/default/db-explain',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/debug/default/download-mail',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/debug/default/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/debug/default/toolbar',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/debug/default/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/faq/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/faq/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/faq/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/faq/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/faq/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/faq/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/galeri/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/galeri/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/galeri/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/galeri/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/galeri/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/galeri/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/gii/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/gii/default/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/gii/default/action',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/gii/default/diff',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/gii/default/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/gii/default/preview',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/gii/default/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/member/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/member/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/member/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/member/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/member/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/member/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/narasi/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/narasi/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/narasi/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/narasi/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/narasi/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/narasi/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/profil/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/profil/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/profil/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/profil/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/profil/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/profil/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/rbac/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/rbac/assignment/*',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/assignment/assign',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/assignment/index',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/assignment/revoke',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/assignment/view',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/default/*',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/default/index',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/menu/*',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/menu/create',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/menu/delete',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/menu/index',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/menu/update',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/menu/view',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/permission/*',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/permission/assign',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/permission/create',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/permission/delete',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/permission/index',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/permission/remove',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/permission/update',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/permission/view',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/role/*',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/role/assign',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/role/create',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/role/delete',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/role/index',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/role/remove',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/role/update',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/role/view',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/route/*',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/route/assign',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/route/create',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/route/index',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/route/refresh',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/route/remove',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/rule/*',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/rule/create',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/rule/delete',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/rule/index',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/rule/update',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/rule/view',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/user/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/rbac/user/activate',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/user/change-password',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/user/delete',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/user/index',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/user/login',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/user/logout',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/user/request-password-reset',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/user/reset-password',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/user/signup',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/rbac/user/view',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/redactor/*',	2,	NULL,	NULL,	NULL,	1496398458,	1496398458),
('/servis-kategori/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/servis-kategori/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/servis-kategori/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/servis-kategori/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/servis-kategori/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/servis-kategori/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/servis/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/servis/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/servis/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/servis/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/servis/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/servis/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/site/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/site/about',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/site/captcha',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/site/change-password',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/site/contact',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/site/error',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/site/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/site/login',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/site/logout',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/site/request-password-reset',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/site/reset-password',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/slider/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/slider/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/slider/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/slider/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/slider/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/slider/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/testimoni/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/testimoni/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/testimoni/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/testimoni/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/testimoni/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/testimoni/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/user/*',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/user/create',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/user/delete',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/user/index',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/user/update',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('/user/view',	2,	NULL,	NULL,	NULL,	1496398459,	1496398459),
('admin',	1,	NULL,	NULL,	NULL,	1496398506,	1496398506),
('member',	1,	NULL,	NULL,	NULL,	1496398528,	1496398528);

DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin',	'/*');

DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `caption` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `config` (`id`, `judul`, `gambar`, `caption`) VALUES
(1,	'SELAMAT DATANG DI NAMUDA ANDALAS',	NULL,	'<p>ini adalah website namuda Andalass<br></p>'),
(2,	'OUR SERVICES',	'',	'ini adalah service yang ada di training kami'),
(3,	'REQUEST A CALLBACK',	'',	'isi nya adalah ini'),
(4,	'LATEST BLOG',	'',	'ini adalah halaman blog'),
(5,	'WORKING HOURS:',	'',	'<ul><li>The this group would somehow form a family is a that\'s the way.</li><li>Mon - Thu: 8.30am to 9.30pm</li><li>Fri - Sat: 9.30am to 9.30pm</li><li>Sunday: <span>Close</span></li></ul>'),
(6,	'TRUSTED PARTNERS',	'',	'Ini adalah partner - partner kami'),
(7,	'About Us',	'images/background/4.jpg',	''),
(8,	'background1',	'images/background/1.jpg',	'Our training is simple to understand, quick to use, and easy to recall <br> because it is practical with no theoretical models'),
(9,	'background2',	'images/background/2.jpg',	''),
(10,	'OUR TEAM MEMBERS',	'',	'Ini Adalah Team Kami'),
(11,	'SERVICES',	'images/background/4.jpg',	''),
(12,	'WHAT WE DO',	'',	'Ini yang kita lakukan'),
(13,	'FAQ',	'images/background/4.jpg',	'FREQUENTLY ASKED QUESTIONS'),
(14,	'OUR BLOG',	'images/background/4.jpg',	'BLOG');

DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanya` text NOT NULL,
  `jawab` text NOT NULL,
  `urutan` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `faq` (`id`, `tanya`, `jawab`, `urutan`) VALUES
(1,	'How do I select whats best for my team?',	'The wood somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.',	1),
(2,	' Am I able to measure the results?',	'The wood somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.',	2),
(3,	'Why Sales Training Consultants?',	'The wood somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.',	3);

DROP TABLE IF EXISTS `galeri`;
CREATE TABLE `galeri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(255) DEFAULT NULL,
  `caption` text,
  `gambar2` varchar(255) DEFAULT NULL,
  `gambar3` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `isi` text,
  `judul2` varchar(255) DEFAULT NULL,
  `isi2` text,
  `judul3` varchar(255) DEFAULT NULL,
  `isi3` text,
  `urutan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `galeri` (`id`, `gambar`, `caption`, `gambar2`, `gambar3`, `youtube`, `kategori`, `judul`, `isi`, `judul2`, `isi2`, `judul3`, `isi3`, `urutan`) VALUES
(8,	'uploads/galeri8a.jpg',	'<p>Ini adalah caption1</p>',	'uploads/galeri8b.jpg',	'',	'https://www.youtube.com/watch?v=0e1uTwSRGgI',	'galeri',	'PROJECT DESCRIPTION',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family.</p><p>This group would somehow form a family that\'s the way the Love Boat promises something for everyone that this matt group would somehow form a the brady bunch that this group they call him Flipper Flipper faster than lightning no one you see is smarter than he maybe you and me think of me once in a while.</p>',	'CONCLUSION',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family.</p><p>This group would somehow form a family that\'s the way the Love Boat promises something for everyone that this matt group would somehow form a the brady bunch that this group they call him Flipper Flipper faster than lightning no one you see is smarter than he maybe you and me think of me once in a while.</p>',	'KEY FEATURES',	'<p>ini adalah isi nyaaaaa</p>',	1),
(9,	'uploads/galeri9a.jpg',	'<p>ini adalah caption2</p>',	'uploads/galeri9b.jpg',	NULL,	'',	'galeri',	'CONCLUSION',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family.</p><p>This group would somehow form a family that\'s the way the Love Boat promises something for everyone that this matt group would somehow form a the brady bunch that this group they call him Flipper Flipper faster than lightning no one you see is smarter than he maybe you and me think of me once in a while.</p>',	NULL,	NULL,	NULL,	NULL,	2),
(10,	'uploads/galeri10a.jpg',	'<p>ini adalah caption 3</p>',	'uploads/galeri10b.jpg',	NULL,	'',	'galeri',	'KEY FEATURES',	'<p>ini adalah isi nyaaaaa</p>',	NULL,	NULL,	NULL,	NULL,	3),
(11,	'uploads/galeri11a.jpg',	'<p>Ini adalah caption 4</p>',	NULL,	NULL,	'',	'galeri',	'Judul 4',	'<p>ini adalah isi 4</p>',	NULL,	NULL,	NULL,	NULL,	4),
(12,	'uploads/galeri12a.jpg',	'<p>ini adalah caption 5</p>',	NULL,	NULL,	'',	'galeri',	'judul 5',	'<p>ini adalah isi 5</p>',	NULL,	NULL,	NULL,	NULL,	5),
(13,	'uploads/galeri13a.jpg',	'<p>ini adalah caption 6</p>',	NULL,	NULL,	'',	'galeri',	'judul 6',	'<p>ini adalah caption 6</p>',	NULL,	NULL,	NULL,	NULL,	6);

DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `nama` varchar(255) NOT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `fb` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `ig` varchar(255) DEFAULT NULL,
  `linkid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `member_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `member` (`id`, `user_id`, `nama`, `jabatan`, `foto`, `fb`, `twitter`, `ig`, `linkid`) VALUES
(1,	NULL,	'SHARA JEANE, SKM',	'DIRECTOR',	'uploads/member1.jpg',	'https://facebook.com/shara',	'twitter.com/shara',	'ig',	'linkid'),
(2,	NULL,	'AFIF RAMDHAN MUDZAKKIR, SKM',	'COMMISSIONER',	'images/resource/afif1.jpg',	'fb',	'twit',	'ig',	'linkid'),
(3,	NULL,	'HEROZI FIRDAUS, SKM',	'PUBLIC RELATION',	'uploads/member3.jpg',	'fb',	'twitter',	'ig',	'linkid'),
(4,	NULL,	'NADYA BALDI, SE',	'FINANCE',	'images/resource/nadya1.jpg',	'fb',	'twit',	'ig',	'linkid'),
(5,	NULL,	'MULYA ANDHIKA PUTRA, ST',	'FINANCE',	'images/resource/mulya1.jpg',	'fb',	'twit',	'ig',	'linkid'),
(6,	NULL,	'ARIF ZAENURY ICHSAN, S.KOM',	'INFORMATION TECHNOLOGY',	'images/resource/arif1.jpg',	'fb',	'twit',	'ig',	'linkid');

DROP TABLE IF EXISTS `narasi`;
CREATE TABLE `narasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `icon` varchar(255) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `narasi` (`id`, `judul`, `isi`, `icon`, `kategori`) VALUES
(1,	'OUR UNIQUE APPROACH',	'The ship set ground on the shore that this group would somehow  family that is  we all became to the brady bunch.',	'flaticon-strategy',	'content1'),
(2,	'OUR VALUES',	'The ship set ground on the shore that this group would somehow  family that is  we all became to the brady bunch.',	'flaticon-monitor',	'content1'),
(3,	'OUR METHODOLOGY',	'The ship set ground on the shore that this group would somehow  family that is  we all became to the brady bunch.',	'flaticon-target',	'content1');

DROP TABLE IF EXISTS `profil`;
CREATE TABLE `profil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `logo1` varchar(255) DEFAULT NULL,
  `logo2` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `profil` (`id`, `nama`, `alamat`, `telp`, `email`, `facebook`, `twitter`, `instagram`, `logo1`, `logo2`, `latitude`, `longitude`) VALUES
(1,	'Namuda - Training Consultant',	'Padang',	'+62 811 694 3553',	'tanyajawab@namudaandalas.com',	'fblink',	'twit',	'ig',	NULL,	'uploads/profil1b.jpg',	'0.5129344',	'101.4184511');

DROP TABLE IF EXISTS `servis`;
CREATE TABLE `servis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `icon` varchar(255) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `servis_kategori_id` int(11) NOT NULL,
  `urutan` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `servis_kategori_id` (`servis_kategori_id`),
  CONSTRAINT `servis_ibfk_1` FOREIGN KEY (`servis_kategori_id`) REFERENCES `servis_kategori` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `servis` (`id`, `nama`, `isi`, `icon`, `gambar`, `servis_kategori_id`, `urutan`) VALUES
(1,	'SALES EFFECTIVENESS',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.</p><p>This group would somehow form a family that\'s the way the Love Boat promises something for everyone that this matt group would somehow form a the brady bunch  that this group they call him Flipper Flipper faster than lightning no one you see is smarter than he maybe you and me think of me once in a while.</p>',	'flaticon-edit-1',	'uploads/servis1.jpg',	1,	1),
(2,	'LEADERSHIP DEVELOPMENT',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.</p>                        <p>This group would somehow form a family that\'s the way the Love Boat promises something for everyone that this matt group would somehow form a the brady bunch  that this group they call him Flipper Flipper faster than lightning no one you see is smarter than he maybe you and me think of me once in a while.</p>',	'flaticon-megaphone',	'images/resource/services-2.jpg',	1,	2),
(3,	'PERSONAL EFFECTIVENESS',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.</p>                        <p>This group would somehow form a family that\'s the way the Love Boat promises something for everyone that this matt group would somehow form a the brady bunch  that this group they call him Flipper Flipper faster than lightning no one you see is smarter than he maybe you and me think of me once in a while.</p>',	'flaticon-school',	'uploads/servis3.png',	1,	3),
(4,	'PRESENTATION SKILLS',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.</p>                        <p>This group would somehow form a family that\'s the way the Love Boat promises something for everyone that this matt group would somehow form a the brady bunch  that this group they call him Flipper Flipper faster than lightning no one you see is smarter than he maybe you and me think of me once in a while.</p>',	'flaticon-monitor',	'images/resource/services-4.jpg',	1,	4),
(5,	'NEGOTIATION TRAINING',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.</p>                        <p>This group would somehow form a family that\'s the way the Love Boat promises something for everyone that this matt group would somehow form a the brady bunch  that this group they call him Flipper Flipper faster than lightning no one you see is smarter than he maybe you and me think of me once in a while.</p>',	'flaticon-pie-chart',	'images/resource/services-5.jpg\"',	1,	5),
(6,	'REAL DEAL COACHING',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way the Love Boat promises something for everyone that this group would somehow form a family that\'s the way we all became the brady bunch  that this group a family that\'s the way the Love Boat promises something for everyone that this group would some is how form a family that\'s the way we all became the brady bunch that this group.</p>                        <p>This group would somehow form a family that\'s the way the Love Boat promises something for everyone that this matt group would somehow form a the brady bunch  that this group they call him Flipper Flipper faster than lightning no one you see is smarter than he maybe you and me think of me once in a while.</p>',	'flaticon-handshake',	'images/resource/services-6.jpg',	1,	6);

DROP TABLE IF EXISTS `servis_kategori`;
CREATE TABLE `servis_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `servis_kategori` (`id`, `nama`) VALUES
(1,	'Tes Kategori');

DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(255) DEFAULT NULL,
  `judul` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `button` varchar(255) DEFAULT NULL,
  `urutan` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `slider` (`id`, `gambar`, `judul`, `caption`, `button`, `urutan`) VALUES
(1,	'uploads/slider1.png',	'WE ARE PALERMO Expertise in Sales Training & <br> Sales Effectiveness Solutions',	'The ship set ground on the shore that this group',	'<a href=\"contact.html\" class=\"theme-btn btn-style-one\">Contact Us</a> &nbsp; &nbsp; <a href=\"contact.html\" class=\"theme-btn btn-style-two\">Read More</a>',	1),
(2,	'images/main-slider/2.jpg',	'PALERMO DELIVERS THE HIGHEST QUALITY <br> & IMPACTFUL LEARNING EXPERIENCE',	'The ship set ground on the shore that this group',	'<a href=\"contact.html\" class=\"theme-btn btn-style-one\">Contact Us</a> &nbsp; &nbsp; <a href=\"contact.html\" class=\"theme-btn btn-style-two\">Read More</a>',	2);

DROP TABLE IF EXISTS `testimoni`;
CREATE TABLE `testimoni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `testimoni` (`id`, `author`, `jabatan`, `isi`, `foto`) VALUES
(1,	'SHARA JEANE',	'Director',	'<p>The ship set ground on the shore that this group would somehow form a family that\'s the way we all became to the brady bunch of this uncharted an right.</p>',	'uploads/testimoni1.jpg'),
(2,	'MULYA ANDHIKA PUTRA, ST',	'Finance',	'Ini Testimoni Mulya Andika Putra',	'images/resource/mulya.jpg'),
(3,	'NADYA BALDI, SE',	'Finance',	'Ini Adalah Testimoni Nadya Baldi, SE',	'images/resource/nadya.jpg'),
(4,	'ISAAC NEWTON',	'Researcher',	'<p>Apel jatuh boleh dimakan</p>',	'uploads/testimoni4.png');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1,	'root',	'cHBEcCozliC4NFmbzNfwPaAN7Vf9-EId',	'$2y$13$igi0dxXDcfN70yuJDY1SjelS9ndmofyetWEQbcFbOoeHQCPOBEPH6',	NULL,	'root@panelmo.com',	10,	1491802147,	1491802147),
(2,	'tester',	'qRlvFIs5UKrnTNwgba4AKtxEKE0tEQHF',	'$2y$13$axbvRxjayHwPQv3bdx7fYee0tbXmrJFWXzauJXeiQpHbwa/3aWYhm',	NULL,	'tester@namuda.com',	10,	1496341642,	1496539344),
(3,	'tester2',	'TDMm-SlboHBsjPTXQ9Lk3iQGTE9JQ5FG',	'$2y$13$MOlBSV8JCsJNWPCEoulh8On5szBqyN6otesoEzIROkRHVETzEJjhW',	NULL,	'tester2@namuda.com',	0,	1496341773,	1496342007);

-- 2017-06-18 19:31:34
