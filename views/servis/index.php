<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\ServisKategori;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Servis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servis-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Servis', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'tableOptions'=> ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions' => ['class' => 'options'],
                'value' => function($model) {
                    return '
                        <a href="'.Url::to(['view', 'id' => $model->id]).'" class="btn btn-xs btn-default option-view"><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="'.Url::to(['update', 'id' => $model->id]).'" class="btn btn-xs btn-default option-update"><i class="glyphicon glyphicon-pencil"></i></a>
                        <a href="'.Url::to(['delete', 'id' => $model->id]).'" class="btn btn-xs btn-default option-delete" data-method="post" data-confirm="Are you sure you want to delete this item?" ><i class="glyphicon glyphicon-trash"></i></a>
                    ';
                }
            ],

            // 'id',
            'nama',
            'isi:html',
            // 'icon',
            // 'gambar',
            [
                'attribute' => 'servis_kategori_id',
                'label'     => 'Kategori',
                'value'     => 'servisKategori.nama',
                'filter'    => ArrayHelper::map(ServisKategori::find()->all(), 'id', 'nama'),
            ],
            // 'urutan',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
