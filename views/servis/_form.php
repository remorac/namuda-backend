<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use app\models\ServisKategori;

/* @var $this yii\web\View */
/* @var $model app\models\Servis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="servis-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        // 'fieldConfig' => [
        //     'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        //     'horizontalCssClasses' => [
        //         'offset' => 'col-sm-offset-3',
        //         'label' => 'col-sm-3',
        //         'wrapper' => 'col-sm-9',
        //         'error' => '',
        //         'hint' => 'col-sm-3',
        //     ],
        // ],
    ]); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'servis_kategori_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(ServisKategori::find()->all(), 'id', 'nama'),
        'options' => ['placeholder' => ''],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'urutan')->textInput() ?>

    <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imageFile')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false
        ]
    ])->hint($model->gambar ? 'File exists.' : ''); ?>
    
    <?= $form->field($model, 'isi')->widget(\yii\redactor\widgets\Redactor::className()) ?>

    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
            <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
