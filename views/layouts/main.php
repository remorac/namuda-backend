<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Alert;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<b>Namuda Andalas</b>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    if (Yii::$app->user->isGuest) {
        // $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = [
            'label' => 'Setting', 
            'items' => [
                ['label' => 'Config', 'url' => ['/config/index']],
                ['label' => 'Profil', 'url' => ['/profil/view', 'id' => '1']],
                ['label' => 'Kategori Artikel', 'url' => ['/artikel-kategori/index']],
                ['label' => 'Kategori Servis', 'url' => ['/servis-kategori/index']],
            ],
        ];

        $menuItems[] = [
            'label' => 'Content', 
            'items' => [
                ['label' => 'Artikel', 'url' => ['/artikel/index']],
                ['label' => 'Komentar', 'url' => ['/artikel-komentar/index']],
                ['label' => 'Galeri', 'url' => ['/galeri/index']],
                ['label' => 'FAQ', 'url' => ['/faq/index']],
                ['label' => 'Narasi', 'url' => ['/narasi/index']],
                ['label' => 'Servis', 'url' => ['/servis/index']],
                ['label' => 'Slider', 'url' => ['/slider/index']],
                ['label' => 'Testimoni', 'url' => ['/testimoni/index']],
            ],
        ];

        $menuItems[] = [
            'label' => 'User', 
            'items' => [
                ['label' => 'Member', 'url' => ['/member/index']],
                ['label' => 'User', 'url' => ['/user/index']],
                // ['label' => 'RBAC', 'url' => ['/rbac/assignment']],
            ],
        ];
        
        $menuItems[] = [
            'label' => 'Current User: '.Yii::$app->user->identity->username, 
            'url' => ['#'], 
            'items' => [
                ['label' => 'Ubah Password', 'url' => ['/site/change-password']],
                ['label' => 'Keluar',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post'],
                ],
            ]
        ];

        /*$menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';*/
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?php
            foreach(Yii::$app->session->getAllFlashes() as $key => $message) {
                $key = $key == "error" ? "danger" : $key;
                echo '<div class="alert alert-' . $key . '">' . $message . "</div>\n";
            }
        ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">Namuda Andalas &copy; <?= date('Y') ?></p>

        <p class="pull-right">developed by <a href="https://remorac.com">remorac.com</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
