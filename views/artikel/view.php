<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Artikel */

$this->title = $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Artikel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artikel-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Komentar', ['artikel-komentar/index', 'ArtikelKomentarSearch[artikel_id]' => $model->id], ['class' => 'btn btn-default pull-right']) ?>
    </p>

    <?= DetailView::widget([
        'options'=> ['class' => 'table', 'style' => 'border-bottom:1px solid #ddd'],
        'template' => "<tr><th width='30%'>{label}</th><td>{value}</td></tr>",
        'model' => $model,
        'attributes' => [
            // 'id',
            'judul',
            'artikelKategori.nama:text:Kategori',
            'user.username:text:Penulis',
            'waktu:datetime',
            [
                'attribute' => 'gambar',
                'format' => 'raw',
                'value' => $model->gambar ? 
                    Html::img($model->gambar, ['height' => '100px', 'style' => 'box-shadow:0 0 2px']) .
                    Html::a('<i class="glyphicon glyphicon-remove"></i>', ['remove-gambar', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-default option-delete pull-right',
                        'title' => 'Remove image',
                        'data' => [
                            'confirm' => 'Are you sure you want to remove this image?',
                            'method' => 'post',
                        ],
                    ]) 
                    : $model->gambar,
            ],
            'isi:html',
        ],
    ]) ?>

</div>
