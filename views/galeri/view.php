<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Galeri */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Galeri', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="galeri-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'options'=> ['class' => 'table', 'style' => 'border-bottom:1px solid #ddd'],
        'template' => "<tr><th width='30%'>{label}</th><td>{value}</td></tr>",
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'gambar',
                'format' => 'raw',
                'value' => $model->gambar ? 
                    Html::img($model->gambar, ['height' => '100px', 'style' => 'box-shadow:0 0 2px']) .
                    Html::a('<i class="glyphicon glyphicon-remove"></i>', ['remove-file', 'id' => $model->id, 'field' => 'gambar'], [
                        'class' => 'btn btn-xs btn-default option-delete pull-right',
                        'title' => 'Remove image',
                        'data' => [
                            'confirm' => 'Are you sure you want to remove this image?',
                            'method' => 'post',
                        ],
                    ]) 
                    : $model->gambar,
            ],
            [
                'attribute' => 'gambar2',
                'format' => 'raw',
                'value' => $model->gambar2 ? 
                    Html::img($model->gambar2, ['height' => '100px', 'style' => 'box-shadow:0 0 2px']) .
                    Html::a('<i class="glyphicon glyphicon-remove"></i>', ['remove-file', 'id' => $model->id, 'field' => 'gambar2'], [
                        'class' => 'btn btn-xs btn-default option-delete pull-right',
                        'title' => 'Remove image',
                        'data' => [
                            'confirm' => 'Are you sure you want to remove this image?',
                            'method' => 'post',
                        ],
                    ]) 
                    : $model->gambar2,
            ],
            [
                'attribute' => 'gambar3',
                'format' => 'raw',
                'value' => $model->gambar3 ? 
                    Html::img($model->gambar3, ['height' => '100px', 'style' => 'box-shadow:0 0 2px']) .
                    Html::a('<i class="glyphicon glyphicon-remove"></i>', ['remove-file', 'id' => $model->id, 'field' => 'gambar3'], [
                        'class' => 'btn btn-xs btn-default option-delete pull-right',
                        'title' => 'Remove image',
                        'data' => [
                            'confirm' => 'Are you sure you want to remove this image?',
                            'method' => 'post',
                        ],
                    ]) 
                    : $model->gambar3,
            ],
            'youtube',
            'kategori',
            'urutan',
            'caption:html',
            'judul',
            'isi:html',
            'judul2',
            'isi2:html',
            'judul3',
            'isi3:html',
        ],
    ]) ?>

</div>
