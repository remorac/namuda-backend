<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GaleriSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="galeri-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'gambar') ?>

    <?= $form->field($model, 'caption') ?>

    <?= $form->field($model, 'gambar2') ?>

    <?= $form->field($model, 'gambar3') ?>

    <?php // echo $form->field($model, 'youtube') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
