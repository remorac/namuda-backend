<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Galeri */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="galeri-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        // 'fieldConfig' => [
        //     'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        //     'horizontalCssClasses' => [
        //         'offset' => 'col-sm-offset-3',
        //         'label' => 'col-sm-3',
        //         'wrapper' => 'col-sm-9',
        //         'error' => '',
        //         'hint' => 'col-sm-3',
        //     ],
        // ],
    ]); ?>

    <?= $form->field($model, 'imageFile1')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false
        ]
    ])->hint($model->gambar ? 'File exists.' : ''); ?>

    <?= $form->field($model, 'imageFile2')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false
        ]
    ])->hint($model->gambar2 ? 'File exists.' : ''); ?>

    <?= $form->field($model, 'imageFile3')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false
        ]
    ])->hint($model->gambar3 ? 'File exists.' : ''); ?>

    <?= $form->field($model, 'youtube')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kategori')->dropDownList([
        'galeri' => 'galeri',
        'client' => 'client',
    ], ['prompt' => '...']) ?>

    <?= $form->field($model, 'urutan')->textInput() ?>

    <?= $form->field($model, 'caption')->widget(\yii\redactor\widgets\Redactor::className()) ?>

    <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isi')->widget(\yii\redactor\widgets\Redactor::className()) ?>

    <?= $form->field($model, 'judul2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isi2')->widget(\yii\redactor\widgets\Redactor::className()) ?>

    <?= $form->field($model, 'judul3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isi3')->widget(\yii\redactor\widgets\Redactor::className()) ?>

    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
            <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
