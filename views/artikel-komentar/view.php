<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ArtikelKomentar */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Komentar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artikel-komentar-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!-- <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?> -->
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'options'=> ['class' => 'table', 'style' => 'border-bottom:1px solid #ddd'],
        'template' => "<tr><th width='30%'>{label}</th><td>{value}</td></tr>",
        'model' => $model,
        'attributes' => [
            'id',
            'artikel.judul:text:Artikel',
            'nama',
            'email:email',
            'isi:ntext',
            'waktu',
        ],
    ]) ?>

</div>
