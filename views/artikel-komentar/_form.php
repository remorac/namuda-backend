<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ArtikelKomentar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="artikel-komentar-form">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        // 'fieldConfig' => [
        //     'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        //     'horizontalCssClasses' => [
        //         'offset' => 'col-sm-offset-3',
        //         'label' => 'col-sm-3',
        //         'wrapper' => 'col-sm-9',
        //         'error' => '',
        //         'hint' => 'col-sm-3',
        //     ],
        // ],
    ]); ?>

    <?= $form->field($model, 'artikel_id')->textInput() ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'waktu')->textInput() ?>

    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
            <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-disk"></i> Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
