<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Artikel;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ArtikelKomentarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Komentar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artikel-komentar-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a('Create Artikel Komentar', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?= GridView::widget([
        'tableOptions'=> ['class' => 'table table-hover'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => '',
                'format' => 'raw',
                'contentOptions' => ['class' => 'options'],
                'value' => function($model) {
                    return '
                        <a href="'.Url::to(['view', 'id' => $model->id]).'" class="btn btn-xs btn-default option-view"><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="'.Url::to(['delete', 'id' => $model->id]).'" class="btn btn-xs btn-default option-delete" data-method="post" data-confirm="Are you sure you want to delete this item?" ><i class="glyphicon glyphicon-trash"></i></a>
                    ';
                }
            ],

            // 'id',
            [
                'attribute' => 'artikel_id',
                'value'     => 'artikel.judul',
                'filter'    => ArrayHelper::map(Artikel::find()->all(), 'id', 'judul'),
            ],
            'nama',
            'email:email',
            'isi:ntext',
            // 'waktu',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
