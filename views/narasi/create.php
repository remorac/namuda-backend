<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Narasi */

$this->title = 'Create Narasi';
$this->params['breadcrumbs'][] = ['label' => 'Narasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="narasi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
