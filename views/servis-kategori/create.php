<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ServisKategori */

$this->title = 'Create Servis Kategori';
$this->params['breadcrumbs'][] = ['label' => 'Servis Kategoris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servis-kategori-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
