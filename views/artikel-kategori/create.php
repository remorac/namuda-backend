<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ArtikelKategori */

$this->title = 'Create Kategori Artikel';
$this->params['breadcrumbs'][] = ['label' => 'Kategori Artikel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artikel-kategori-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
