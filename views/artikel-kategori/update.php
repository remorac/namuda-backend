<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ArtikelKategori */

$this->title = 'Update Kategori Artikel: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kategori Artikel', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="artikel-kategori-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
