<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Member */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'options'=> ['class' => 'table', 'style' => 'border-bottom:1px solid #ddd'],
        'template' => "<tr><th width='30%'>{label}</th><td>{value}</td></tr>",
        'model' => $model,
        'attributes' => [
            'id',
            'user.username:text:User',
            'nama',
            'jabatan',
            [
                'attribute' => 'foto',
                'format' => 'raw',
                'value' => $model->foto ? 
                    Html::img($model->foto, ['height' => '100px', 'style' => 'box-shadow:0 0 2px']) .
                    Html::a('<i class="glyphicon glyphicon-remove"></i>', ['remove-foto', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-default option-delete pull-right',
                        'title' => 'Remove image',
                        'data' => [
                            'confirm' => 'Are you sure you want to remove this image?',
                            'method' => 'post',
                        ],
                    ]) 
                    : $model->foto,
            ],
            'fb',
            'twitter',
            'ig',
            'linkid',
        ],
    ]) ?>

</div>
