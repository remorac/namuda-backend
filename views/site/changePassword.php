<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-changepassword">
    <h1><?= Html::encode($this->title) ?></h1>
   
    <p>Please fill out the following fields to change your password: </p>
   
    <?php $form = ActiveForm::begin([
        'id'=>'changepassword-form',
        'layout' => 'horizontal',
        // 'fieldConfig' => [
        //     'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        //     'horizontalCssClasses' => [
        //         'offset' => 'col-sm-offset-3',
        //         'label' => 'col-sm-3',
        //         'wrapper' => 'col-sm-9',
        //         'error' => '',
        //         'hint' => 'col-sm-3',
        //     ],
        // ],
    ]); ?>

    <?= $form->field($model,'oldpass',['inputOptions'=>[
        'placeholder'=>''
    ]])->passwordInput() ?>
   
    <?= $form->field($model,'newpass',['inputOptions'=>[
        'placeholder'=>''
    ]])->passwordInput() ?>
   
    <?= $form->field($model,'repeatnewpass',['inputOptions'=>[
        'placeholder'=>''
    ]])->passwordInput() ?>
   
    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
            <?= Html::submitButton('Change',[
                'class'=>'btn btn-primary'
            ]) ?>
        </div>
    </div>
    
    <?php ActiveForm::end(); ?>
</div>