<?php

/* @var $this yii\web\View */

$this->title = 'Namuda Andalas';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome!</h1>

        <p class="lead">Admin Panel of Namuda Andalas Website.</p>

        <?php if (!Yii::$app->user->isGuest) { ?>
        <p><a class="btn btn-lg btn-success" href="<?= \yii\helpers\Url::to(['artikel/create']) ?>">Create New Article</a></p>
        <?php } ?>
    </div>
</div>
