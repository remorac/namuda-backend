<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "galeri".
 *
 * @property int $id
 * @property string $gambar
 * @property string $caption
 * @property string $gambar2
 * @property string $gambar3
 * @property string $youtube
 * @property string $kategori
 */
class Galeri extends \yii\db\ActiveRecord
{
    public $imageFile1;
    public $imageFile2;
    public $imageFile3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'galeri';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['caption', 'isi', 'isi2', 'isi3'], 'string'],
            [['gambar', 'gambar2', 'gambar3', 'youtube', 'kategori', 'judul', 'judul2', 'judul3'], 'string', 'max' => 255],
            [['urutan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gambar' => 'Gambar',
            'caption' => 'Caption',
            'gambar2' => 'Gambar2',
            'gambar3' => 'Gambar3',
            'youtube' => 'Youtube',
            'kategori' => 'Kategori',
            'judul' => 'Judul',
            'judul2' => 'Judul 2',
            'judul3' => 'Judul 3',
            'isi' => 'Isi',
            'isi2' => 'Isi 2',
            'isi3' => 'Isi 3',
            'imageFile1' => 'Gambar',
            'imageFile2' => 'Gambar 2',
            'imageFile3' => 'Gambar 3',
        ];
    }
}
