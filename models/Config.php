<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "config".
 *
 * @property int $id
 * @property string $judul
 * @property string $gambar
 * @property string $caption
 */
class Config extends \yii\db\ActiveRecord
{
    public $imageFile;
 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'caption'], 'required'],
            [['caption'], 'string'],
            [['judul', 'gambar'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'gambar' => 'Gambar',
            'caption' => 'Caption',
            'imageFile' => 'Gambar',
        ];
    }
}
