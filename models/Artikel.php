<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "artikel".
 *
 * @property int $id
 * @property string $judul
 * @property int $artikel_kategori_id
 * @property int $user_id
 * @property string $waktu
 * @property string $isi
 * @property string $gambar
 *
 * @property ArtikelKategori $artikelKategori
 * @property User $user
 * @property ArtikelKomentar[] $artikelKomentars
 */
class Artikel extends \yii\db\ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'artikel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'user_id', 'isi'], 'required'],
            [['artikel_kategori_id', 'user_id'], 'integer'],
            [['waktu'], 'safe'],
            [['isi'], 'string'],
            [['judul', 'gambar'], 'string', 'max' => 255],
            [['artikel_kategori_id'], 'exist', 'skipOnError' => true, 'targetClass' => ArtikelKategori::className(), 'targetAttribute' => ['artikel_kategori_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'artikel_kategori_id' => 'Kategori',
            'user_id' => 'User ID',
            'waktu' => 'Waktu',
            'isi' => 'Isi',
            'gambar' => 'Gambar',
            'imageFile' => 'Gambar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtikelKategori()
    {
        return $this->hasOne(ArtikelKategori::className(), ['id' => 'artikel_kategori_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtikelKomentars()
    {
        return $this->hasMany(ArtikelKomentar::className(), ['artikel_id' => 'id']);
    }
}
