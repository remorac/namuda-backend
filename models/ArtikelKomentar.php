<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "artikel_komentar".
 *
 * @property int $id
 * @property int $artikel_id
 * @property string $nama
 * @property string $email
 * @property string $isi
 * @property string $waktu
 *
 * @property Artikel $artikel
 */
class ArtikelKomentar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'artikel_komentar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['artikel_id', 'nama', 'email', 'isi'], 'required'],
            [['artikel_id'], 'integer'],
            [['isi'], 'string'],
            [['waktu'], 'safe'],
            [['nama', 'email'], 'string', 'max' => 255],
            [['artikel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Artikel::className(), 'targetAttribute' => ['artikel_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'artikel_id' => 'Artikel',
            'nama' => 'Nama',
            'email' => 'Email',
            'isi' => 'Isi',
            'waktu' => 'Waktu',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtikel()
    {
        return $this->hasOne(Artikel::className(), ['id' => 'artikel_id']);
    }
}
