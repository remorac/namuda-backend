<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profil".
 *
 * @property int $id
 * @property string $nama
 * @property string $alamat
 * @property string $telp
 * @property string $email
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $logo1
 * @property string $logo2
 * @property string $latitude
 * @property string $longitude
 */
class Profil extends \yii\db\ActiveRecord
{

    public $imageFile1;
    public $imageFile2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama', 'alamat', 'telp', 'email', 'facebook', 'twitter', 'instagram', 'logo1', 'logo2', 'latitude', 'longitude'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'telp' => 'Telp',
            'email' => 'Email',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'logo1' => 'Logo1',
            'logo2' => 'Logo2',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'imageFile1' => 'Logo 1',
            'imageFile2' => 'Logo 2',
        ];
    }
}
