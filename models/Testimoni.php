<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "testimoni".
 *
 * @property int $id
 * @property string $author
 * @property string $jabatan
 * @property string $isi
 * @property string $foto
 */
class Testimoni extends \yii\db\ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'testimoni';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author', 'jabatan', 'isi'], 'required'],
            [['isi'], 'string'],
            [['author', 'jabatan', 'foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author' => 'Author',
            'jabatan' => 'Jabatan',
            'isi' => 'Isi',
            'foto' => 'Foto',
            'imageFile' => 'Foto',
        ];
    }
}
