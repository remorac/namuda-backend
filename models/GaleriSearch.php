<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Galeri;

/**
 * GaleriSearch represents the model behind the search form of `app\models\Galeri`.
 */
class GaleriSearch extends Galeri
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'urutan'], 'integer'],
            [['gambar', 'caption', 'gambar2', 'gambar3', 'youtube', 'kategori', 'judul', 'isi', 'judul2', 'isi2', 'judul3', 'isi3'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Galeri::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'urutan' => $this->urutan,
        ]);

        $query->andFilterWhere(['like', 'gambar', $this->gambar])
            ->andFilterWhere(['like', 'caption', $this->caption])
            ->andFilterWhere(['like', 'gambar2', $this->gambar2])
            ->andFilterWhere(['like', 'gambar3', $this->gambar3])
            ->andFilterWhere(['like', 'youtube', $this->youtube])
            ->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'isi', $this->isi])
            ->andFilterWhere(['like', 'judul2', $this->judul2])
            ->andFilterWhere(['like', 'isi2', $this->isi2])
            ->andFilterWhere(['like', 'judul3', $this->judul3])
            ->andFilterWhere(['like', 'isi3', $this->isi3]);

        return $dataProvider;
    }
}
