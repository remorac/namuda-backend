<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property int $id
 * @property int $user_id
 * @property string $nama
 * @property string $jabatan
 * @property string $foto
 * @property string $fb
 * @property string $twitter
 * @property string $ig
 * @property string $linkid
 *
 * @property User $user
 */
class Member extends \yii\db\ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['nama'], 'required'],
            [['nama', 'jabatan', 'foto', 'fb', 'twitter', 'ig', 'linkid'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'nama' => 'Nama',
            'jabatan' => 'Jabatan',
            'foto' => 'Foto',
            'fb' => 'Fb',
            'twitter' => 'Twitter',
            'ig' => 'Ig',
            'linkid' => 'Linkid',
            'imageFile' => 'Foto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
