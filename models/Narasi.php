<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "narasi".
 *
 * @property int $id
 * @property string $judul
 * @property string $isi
 * @property string $icon
 * @property string $kategori
 */
class Narasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'narasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'isi', 'icon', 'kategori'], 'required'],
            [['isi'], 'string'],
            [['judul', 'icon', 'kategori'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'isi' => 'Isi',
            'icon' => 'Icon',
            'kategori' => 'Kategori',
        ];
    }
}
