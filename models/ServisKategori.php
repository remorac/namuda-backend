<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servis_kategori".
 *
 * @property int $id
 * @property string $nama
 *
 * @property Servis[] $servis
 */
class ServisKategori extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servis_kategori';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServis()
    {
        return $this->hasMany(Servis::className(), ['servis_kategori_id' => 'id']);
    }
}
