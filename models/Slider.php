<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property string $gambar
 * @property string $judul
 * @property string $caption
 * @property string $button
 * @property int $urutan
 */
class Slider extends \yii\db\ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'caption', 'urutan'], 'required'],
            [['urutan'], 'integer'],
            [['gambar', 'judul', 'caption', 'button'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gambar' => 'Gambar',
            'judul' => 'Judul',
            'caption' => 'Caption',
            'button' => 'Button',
            'urutan' => 'Urutan',
            'imageFile' => 'Gambar',
        ];
    }
}
