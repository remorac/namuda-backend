<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servis".
 *
 * @property int $id
 * @property string $nama
 * @property string $isi
 * @property string $icon
 * @property string $gambar
 * @property int $servis_kategori_id
 * @property int $urutan
 *
 * @property ServisKategori $servisKategori
 */
class Servis extends \yii\db\ActiveRecord
{
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'isi', 'icon', 'servis_kategori_id', 'urutan'], 'required'],
            [['isi'], 'string'],
            [['servis_kategori_id', 'urutan'], 'integer'],
            [['nama', 'icon', 'gambar'], 'string', 'max' => 255],
            [['servis_kategori_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServisKategori::className(), 'targetAttribute' => ['servis_kategori_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'isi' => 'Isi',
            'icon' => 'Icon',
            'gambar' => 'Gambar',
            'servis_kategori_id' => 'Kategori',
            'urutan' => 'Urutan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServisKategori()
    {
        return $this->hasOne(ServisKategori::className(), ['id' => 'servis_kategori_id']);
    }
}
