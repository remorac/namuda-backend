<?php

namespace app\controllers;

use Yii;
use app\models\Testimoni;
use app\models\TestimoniSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

/**
 * TestimoniController implements the CRUD actions for Testimoni model.
 */
class TestimoniController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'remove-foto' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Testimoni models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TestimoniSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Testimoni model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Testimoni model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Testimoni();

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->save();

            if (file_exists($model->foto)) unlink($model->foto);
            if ($model->imageFile) {
                $model->foto = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'.'.$model->imageFile->extension;
                $model->save();
                $model->imageFile->saveAs($model->foto);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Testimoni model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->save();
            
            if ($model->imageFile) {
                if (file_exists($model->foto)) unlink($model->foto);
                $model->foto = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'.'.$model->imageFile->extension;
                $model->save();
                $model->imageFile->saveAs($model->foto);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Testimoni model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model  = $this->findModel($id);
        if (file_exists($model->foto)) unlink($model->foto);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Testimoni model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Testimoni the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Testimoni::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRemoveFoto($id)
    {
        $model = $this->findModel($id);
        if (file_exists($model->foto)) unlink($model->foto);
        $model->foto = null;
        $model->save();

        return $this->redirect(['view', 'id' => $model->id]);
    }
}
