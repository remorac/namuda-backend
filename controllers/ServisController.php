<?php

namespace app\controllers;

use Yii;
use app\models\Servis;
use app\models\ServisSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

/**
 * ServisController implements the CRUD actions for Servis model.
 */
class ServisController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'remove-gambar' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Servis models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Servis model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Servis model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Servis();

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->save();

            if (file_exists($model->gambar)) unlink($model->gambar);
            if ($model->imageFile) {
                $model->gambar = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'.'.$model->imageFile->extension;
                $model->save();
                $model->imageFile->saveAs($model->gambar);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Servis model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->save();
            
            if ($model->imageFile) {
                if (file_exists($model->gambar)) unlink($model->gambar);
                $model->gambar = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'.'.$model->imageFile->extension;
                $model->save();
                $model->imageFile->saveAs($model->gambar);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Servis model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (file_exists($model->gambar)) unlink($model->gambar);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Servis model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Servis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Servis::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRemoveGambar($id)
    {
        $model = $this->findModel($id);
        if (file_exists($model->gambar)) unlink($model->gambar);
        $model->gambar = null;
        $model->save();

        return $this->redirect(['view', 'id' => $model->id]);
    }
}
