<?php

namespace app\controllers;

use Yii;
use app\models\Galeri;
use app\models\GaleriSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

/**
 * GaleriController implements the CRUD actions for Galeri model.
 */
class GaleriController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'remove-file' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Galeri models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GaleriSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Galeri model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Galeri model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Galeri();

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile1 = UploadedFile::getInstance($model, 'imageFile1');
            $model->imageFile2 = UploadedFile::getInstance($model, 'imageFile2');
            $model->imageFile3 = UploadedFile::getInstance($model, 'imageFile3');
            $model->save();

            if (file_exists($model->gambar)) unlink($model->gambar);
            if (file_exists($model->gambar2)) unlink($model->gambar2);
            if (file_exists($model->gambar3)) unlink($model->gambar3);
            if ($model->imageFile1) {
                $model->gambar = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'a.'.$model->imageFile1->extension;           
                $model->save();
                $model->imageFile1->saveAs($model->gambar);
            }
            if ($model->imageFile2) {
                $model->gambar2 = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'b.'.$model->imageFile2->extension;           
                $model->save();
                $model->imageFile2->saveAs($model->gambar2);
            }
            if ($model->imageFile3) {
                $model->gambar3 = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'c.'.$model->imageFile3->extension;          
                $model->save();
                $model->imageFile3->saveAs($model->gambar3);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Galeri model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile1 = UploadedFile::getInstance($model, 'imageFile1');
            $model->imageFile2 = UploadedFile::getInstance($model, 'imageFile2');
            $model->imageFile3 = UploadedFile::getInstance($model, 'imageFile3');
            $model->save();
            
            if ($model->imageFile1) {
                if (file_exists($model->gambar)) unlink($model->gambar);
                $model->gambar = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'a.'.$model->imageFile1->extension;           
                $model->save();
                $model->imageFile1->saveAs($model->gambar);
            }
            if ($model->imageFile2) {
                if (file_exists($model->gambar2)) unlink($model->gambar2);
                $model->gambar2 = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'b.'.$model->imageFile2->extension;           
                $model->save();
                $model->imageFile2->saveAs($model->gambar2);
            }
            if ($model->imageFile3) {
                if (file_exists($model->gambar3)) unlink($model->gambar3);
                $model->gambar3 = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'c.'.$model->imageFile3->extension;          
                $model->save();
                $model->imageFile3->saveAs($model->gambar3);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Galeri model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model  = $this->findModel($id);
        if (file_exists($model->gambar)) unlink($model->gambar);
        if (file_exists($model->gambar2)) unlink($model->gambar2);
        if (file_exists($model->gambar3)) unlink($model->gambar3);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Galeri model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Galeri the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Galeri::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRemoveFile($id, $field)
    {
        $model = $this->findModel($id);
        if (file_exists($model->$field)) unlink($model->$field);
        $model->$field = null;
        $model->save();

        return $this->redirect(['view', 'id' => $model->id]);
    }
}
