<?php

namespace app\controllers;

use Yii;
use app\models\Profil;
use app\models\ProfilSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

/**
 * ProfilController implements the CRUD actions for Profil model.
 */
class ProfilController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'remove-file' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Profil models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProfilSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Profil model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Profil model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Profil();

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile1 = UploadedFile::getInstance($model, 'imageFile1');
            $model->imageFile2 = UploadedFile::getInstance($model, 'imageFile2');
            $model->save();

            if (file_exists($model->logo1)) unlink($model->logo1);
            if (file_exists($model->logo2)) unlink($model->logo2);
            if ($model->imageFile1) {
                $model->logo1 = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'a.'.$model->imageFile1->extension;
                $model->save();
                $model->imageFile1->saveAs($model->logo1);
            }
            if ($model->imageFile2) {
                $model->logo2 = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'b.'.$model->imageFile2->extension;
                $model->save();
                $model->imageFile2->saveAs($model->logo2);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Profil model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile1 = UploadedFile::getInstance($model, 'imageFile1');
            $model->imageFile2 = UploadedFile::getInstance($model, 'imageFile2');
            $model->save();
            
            if ($model->imageFile1) {
                if (file_exists($model->logo1)) unlink($model->logo1);
                $model->logo1 = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'a.'.$model->imageFile1->extension;
                $model->save();
                $model->imageFile1->saveAs($model->logo1);
            }
            if ($model->imageFile2) {
                if (file_exists($model->logo2)) unlink($model->logo2);
                $model->logo2 = Yii::$app->params['uploadLocation'].$model->tableName().$model->id.'b.'.$model->imageFile2->extension;
                $model->save();
                $model->imageFile2->saveAs($model->logo2);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Profil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model  = $this->findModel($id);
        if (file_exists($model->logo1)) unlink($model->logo1);
        if (file_exists($model->logo2)) unlink($model->logo2);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Profil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Profil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profil::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRemoveFile($id, $field)
    {
        $model = $this->findModel($id);
        if (file_exists($model->$field)) unlink($model->$field);
        $model->$field = null;
        $model->save();

        return $this->redirect(['view', 'id' => $model->id]);
    }
}
